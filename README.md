# Projet Agence de biens

Si composer n'est pas installer sur votre système :
```
wget https://raw.githubusercontent.com/composer/getcomposer.org/1b137f8bf6db3e79a38a5bc45324414a6b1f9df2/web/installer -O - -q | php -- --quiet
```
Pour installer les dépendances :
```
composer install
```
ou
```
php composer.phar install
```

Pour initialiser la base de données :
```
php bin/console make:migration
php bin/console doctrine:migrations:migrate
```

Pour remplir la base de données de valeur fictives :
```
php bin/console doctrine:fixtures:load
```

Pour lancer le serveur :
```
php bin/console server:run
```

## Développeur

* **Romain HERAULT** - *Master* - [r.herault](http://rherault.fr)