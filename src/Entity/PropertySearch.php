<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

class PropertySearch{

  private $maxPrice;

  /**
   * @Assert\Range(min=10, max=400)
   */
  private $minSurface;

  private $options;


  public function __construct(){
    $this->options = new ArrayCollection();
  }

  public function getMaxPrice(){
    return $this->maxPrice;
  }

  public function setMaxPrice(int $maxPrice){
    $this->maxPrice = $maxPrice;
    return $this;
  }

  public function getMinSurface(){
    return $this->minSurface;
  }

  public function setMinSurface(int $minSurface){
    $this->minSurface = $minSurface;
    return $this;
  }

  public function getOptions(){
    return $this->options;
  }

  public function setOptions(ArrayCollection $options){
    $this->options = $options;
  }
}