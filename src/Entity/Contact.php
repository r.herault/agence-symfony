<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Contact {

  /**
   * @Assert\NotBlank()
   * @Assert\Length(min=2, max=100)
   */
  private $firstname;

  /**
   * @Assert\NotBlank()
   * @Assert\Length(min=2, max=100)
   */
  private $lastname;

  /**
   * @Assert\NotBlank()
   * @Assert\Regex(
   * pattern="/[0-9]{10}/"
   * )
   */
  private $phone;

  /**
   * @Assert\NotBlank()
   * @Assert\Email()
   */
  private $email;

  /**
   * @Assert\NotBlank()
   * @Assert\Length(min=10)
   */
  private $message;

  private $property;

  public function getFirstname(){
    return $this->firstname;
  }

  public function setFirstname(string $firstname){
    $this->firstname = $firstname;
    return $this;
  }

  public function getLastname(){
    return $this->lastname;
  }

  public function setLastname(string $lastname){
    $this->lastname = $lastname;
    return $this;
  }

  public function getPhone(){
    return $this->phone;
  }

  public function setphone(int $phone){
    $this->phone = $phone;
    return $this;
  }

  public function getEmail(){
    return $this->email;
  }

  public function setEmail(string $email){
    $this->email = $email;
    return $this;
  }

  public function getMessage(){
    return $this->message;
  }

  public function setMessage(string $message){
    $this->message = $message;
    return $this;
  }

  public function getProperty(){
    return $this->property;
  }

  public function setProperty(Property $property){
    $this->property = $property;
    return $this;
  }
}